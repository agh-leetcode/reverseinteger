import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

public class Solution {

 //   private boolean checked = true;

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int x = 123;
        int y = -123;
        int z = 120;
        int err = 1534236469;
        int err2 = -2147483648;
        int result = reverse(z);
        System.out.println(result);
    }

    //simple solution long value
    private int reverse(int input){
        long result = 0;

        while (input!=0){
            result = result * 10 + input % 10;
            input /= 10;
        }

        if (result < Integer.MIN_VALUE || result > Integer.MAX_VALUE){
            return 0;
        }

        return (int) result;

    }

/*    private int reverse(int input) {
        int reversedNum = 0;
        if (String.valueOf(input).length()>=10)
            checked =false;
        for (int i = input; i != 0; i /= 10) {
            reversedNum = reversedNum * 10 + i % 10;
        }
        if (!checked){
            checked = true;
            if (!checkIfMatches(input, reversedNum))
                return 0;
        }
        return reversedNum;
    }

    private boolean checkIfMatches(int input, int reversedNum){
        int reversedInput = 0;
        for (int i = reversedNum; i != 0; i /= 10) {
            reversedInput = reversedInput * 10 + i % 10;
        }
        return reversedInput == input;
    }*/
}
